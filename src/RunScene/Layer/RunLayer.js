var RunLayer = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 
		
		this.callNext.retain();
		this.callKill.retain();
		//时钟
	//	this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		
//		var mukuai = new cc.Sprite("#iron/mk.png");
//		mukuai.setPosition(800,200);
//		this.addChild(mukuai);
		

	},
	loadCuo:function(pos){
		var cuo = new Cuonode(this);
		this.loadInLib(cuo, pos,cc.p(600,300));
		
	},
	loadZhi:function(pos){
		var zhi = new Zhi(this);
		this.loadInLib(zhi, pos,cc.p(750,200));
	},
	loadShiguan:function(pos){
		var shiguan = new Shiguan(this);
		this.loadInLib(shiguan, pos,cc.p(1100,200));
	},
	loadDaoguan:function(pos){
		var daoguan =  new Button(this, 10, TAG_DAOGUAN,"#daoguan.png", this.callback);
		//var daoguan = new cc.Sprite("#daoguan.png");
		//this.addChild(daoguan,10);
		daoguan.setScale(0.8);
		daoguan.setRotation(-5);
		daoguan.setPosition(pos);
		var move = cc.moveTo(1,cc.p(400,280));
		var move1 = cc.moveTo(1,cc.p(560,290));
		var seq = cc.sequence(move,move1,cc.callFunc(function(){
			this.flowNext();
		},this));
		daoguan.runAction(seq);
		//this.loadInLib(daoguan, pos,cc.p(500,200));
	},
	loadIron:function(pos){
		var iron = new Iron(this);
		this.loadInLib(iron, pos,cc.p(610,300));
	},
	loadLamp:function(pos){
		var lamp = new Lampnode(this);
		this.loadInLib(lamp, pos,cc.p(800+50,180));
	},
	loadH2:function(pos){		
		var ch4 =  new Button(this, 10, TAG_H2,"#h2.png", this.callback);
		this.loadInLib(ch4, pos,cc.p(250,210));
		var show = new ShowTip("点燃酒精灯前需要先通" +
				"\n入氢气，排除试管中的" +
				"\n空气，以防爆炸",25,cc.p(390,380));
		var seq = cc.sequence(cc.delayTime(0.8),cc.callFunc(function(){
			this.schedule(this.loadArrow, 0.4);
			//this.loadArrow();
		},this));
		ch4.runAction(seq);
	},
	loadMatch:function(pos){
		var match = new Match(this);
		match.setPosition(pos);
		match.setRotation(-30);
		match.runAction(cc.moveTo(1,cc.p(830+50,190)));
//
//		var show = new ShowTip(this,"观察甲烷气体燃烧" +
//				"\n火焰的颜色和形状",25,cc.p(400,300),cc.p(400,300),300,100,4);
	},
	loadArrow:function(){	
		var ch4=this.getChildByTag(TAG_H2);
		var arrow=new cc.Sprite("#arrow1.png");		
		this.addChild(arrow,3,TAG_ARROW);
		arrow.setPosition(cc.p(520,316));
		arrow.setRotation(171);
		var move=cc.moveTo(0.6, cc.p(650,326));
		var seq=cc.sequence(move,cc.callFunc(function() {
			arrow.removeFromParent();
		}, this),cc.delayTime(2),cc.callFunc(function() {

		}, this));
		arrow.runAction(seq);
	},
	stopArrow:function(){
		this.unschedule(this.loadArrow);
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},
	
	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){
		 
		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});