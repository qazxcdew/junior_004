Cuo = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_CUO_NODE);
		this.init();
	},

	init : function(){
		var cuo =  new Button(this, 10, TAG_CUO, "#CuO.png",this.callback);
		var label = new cc.LabelTTF("CuO","微软雅黑",13);
		label.setPosition(22,42);
		label.setColor(cc.color(0,0,0));
		cuo.addChild(label);
		
		var lid =  new Button(this, 9, TAG_CUOLID, "#lid.png",this.callback);
        lid.setScale(1.7);
        lid.setPosition(0,58 );
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_CUOLID:
			p.setSpriteFrame("lid5.png");
			p.setPosition(cc.p(0,40));
			var move = cc.moveBy(0.5,cc.p(0,30));
			var move1 = cc.moveBy(1,cc.p(-50,-100));
			var rotate = cc.rotateTo(1,-180);
			var spawn = cc.spawn(move1,rotate);
			var seq = cc.sequence(move,spawn,cc.callFunc(function(){
				this.flowNext();
				var show = new ShowTip("注意瓶塞朝上",25,cc.p(510,200));
				//p.removeFromParent(true);

			},this));
			p.runAction(seq);
			break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});