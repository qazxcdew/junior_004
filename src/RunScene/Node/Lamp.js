Lamp = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_LAMP_NODE);
		this.init();
	},

	init : function(){
		var lamp =  new Button(this, 9, TAG_LAMP, "#lamp/lp1.png",this.callback);
	
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_LAMP:
			if(action==ACTION_DO1){
				hand.addrighthand(this,"#hand/hand_right",cc.p(20,-25));
				var show = new ShowTip("使试管底部" +
						"\n受热均匀",25,cc.p(850,370));
				var ber = cc.bezierTo(1,[cc.p(-20,50),cc.p(-100,60),cc.p(-130,80)]);
				var move = cc.moveTo(0.5,cc.p(-80-50,80));
				var move1 = cc.moveTo(0.5,cc.p(-50-50,90));
				var seq = cc.sequence(ber,move1,move,move1,move,move1,cc.callFunc(function(){
					hand.removehand(this,1,2);
					this.flowNext();
				},this));
				this.runAction(seq);
			}
			else if(action==ACTION_DO2){
				hand.addrighthand(this,"#hand/hand_right",cc.p(20,-25));
				var show = new ShowTip("继续通入氢气以" +
						"\n防铜重新被氧化",25,cc.p(900,370));
				var move = cc.moveTo(0.5,cc.p(0,0));
				var ber = cc.bezierTo(0.5,[cc.p(-50,60),cc.p(-20,40),cc.p(0,0)]);
				//var move1 = cc.moveTo(0.5,cc.p(-50,80));
				var seq = cc.sequence(ber,cc.callFunc(function(){
					hand.removehand(this,1,2);
					this.flowNext();
				},this));
				this.runAction(seq);
			}
			
		}

	},

	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		case TAG_LAMP:
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});