/**
 * 药品库
 */
Lib = cc.Sprite.extend({
	libArr: [],
	openFlag:false,
	doing:false,
	rm : 5,
	ctor:function(p){
		this._super("#lib_back.png");
		p.addChild(this,30);
		this.setTag(TAG_LIB);
		this.init();
	},
	init:function(){
		this.libArr = [];
		this.openFlag = false;
		this.setPosition(gg.width + this.width * 0.5 + 9,550 + 30);

		var cuo = new LibButton(this, 10, TAG_LIB_CUO,"#CuO.png", this.callback);
		cuo.setPosition(30, this.height * 0.5);
		
		var zhi = new LibButton(this, 10, TAG_LIB_ZHI,"#zhi.png", this.callback);
		zhi.right(cuo, this.rm);
		
		var shiguan = new LibButton(this, 10, TAG_LIB_SHIGUAN,"#sg.png", this.callback);
		shiguan.right(zhi, this.rm);
		
		var daoguan = new LibButton(this, 10, TAG_LIB_DAOGUAN,"#daoguan.png", this.callback);
		daoguan.right(shiguan, this.rm);
		
		var iron = new LibButton(this, 10, TAG_LIB_IRON,"#iron.png", this.callback);
		iron.right(daoguan, this.rm);
		
		var h2 = new LibButton(this, 10, TAG_LIB_H2,"#h2.png", this.callback);
		h2.right(iron, this.rm);
		
		var lamp  = new LibButton(this, 10, TAG_LIB_LAMP,"#lamp/lamp.png", this.callback);
		lamp.right(h2, this.rm);
		
		var match  = new LibButton(this, 10, TAG_LIB_MATCH,"#match.png", this.callback);
		match.right(lamp, this.rm);


	},
	moveLib:function(tag,width){
		width = 75;
		var begin = false;
		for(var i in this.libArr){
			var libTag = this.libArr[i];
			if(tag == libTag){
				begin = true;
			}
			if(begin){
				var lib = this.getChildByTag(libTag);
				if(lib != null){
					lib.runAction(cc.moveBy(0.5,cc.p(-width, 0)));
				}
			}
		}
	},
	callback:function(p){
		var pos = this.getPosition(); 
		var action = gg.flow.flow.action;
		switch(p.getTag()){
			
			case TAG_LIB_CUO:
				ll.run.loadCuo(pos);	
				break;
			case TAG_LIB_ZHI:
			    ll.run.loadZhi(pos);	
			    break;
			case TAG_LIB_DAOGUAN:
			    ll.run.loadDaoguan(pos);	
			    break;
			case TAG_LIB_IRON:
			    ll.run.loadIron(pos);	
			    break;
			case TAG_LIB_H2:
			    ll.run.loadH2(pos);	
			    break;	
			case TAG_LIB_LAMP:
			    ll.run.loadLamp(pos);	
			    break;
			case TAG_LIB_MATCH:
			    ll.run.loadMatch(pos);	
			    break;	
			case TAG_LIB_SHIGUAN:
			    ll.run.loadShiguan(pos);	
			    break;	
			
			
			default:
				break;
		}
		if(action == ACTION_NONE){
			this.moveLib(p.getTag(), p.width * p.getScale());
			p.removeFromParent(true);
		}
	},
	isOpen:function(){
		return this.openFlag; 
	},
	open:function(){
		if(this.openFlag || this.doing){
			return;
		}
		this.doing = true;
		var move = cc.moveBy(0.4, cc.p(-this.width,0));
		var func = cc.callFunc(function(){
			this.openFlag = true;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			 if(tag instanceof Array){
				 if(TAG_LIB_MIN < tag[1]){
					 // 显示箭头
					 gg.flow.location();
				 }
			 }
		}, this);
		var seq = cc.sequence(move,func);
		this.runAction(seq);
	},
	close:function(){
		if(!this.openFlag || this.doing){
			return;
		}
		this.doing = true;
		var move = cc.moveBy(0.4, cc.p(this.width,0));
		var func = cc.callFunc(function(){
			this.openFlag = false;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			if(tag instanceof Array){
				if(TAG_LIB_MIN < tag[1]){
					// 隐藏箭头
					//ll.tip.arr.out();
					//ll.tip.arr.setPosition(gg.width-45,455);
					ll.tip.arr.pos(ll.tool.getChildByTag(TAG_BUTTON_LIB));
				}
			}
		}, this);
		var seq = cc.sequence(move,func);
		this.runAction(seq);
	}
});
TAG_LIB_MIN = 30000;

TAG_LIB_CUO = 30001;
TAG_LIB_ZHI=30002;
TAG_LIB_DAOGUAN=30003;
TAG_LIB_H2=30004;
TAG_LIB_MATCH=30005;
TAG_LIB_IRON=30006;
TAG_LIB_LAMP=30007;
TAG_LIB_SHIGUAN=30008;






libRelArr = [
     {tag:TAG_LIB_MIN, name:""},   
     {tag:TAG_LIB_CUO, name:"氧化铜"},
     {tag:TAG_LIB_ZHI,name:"V型纸槽"},
     {tag:TAG_LIB_DAOGUAN,name:"导管"},
     {tag:TAG_LIB_H2,name:"H2"},
     {tag:TAG_LIB_MATCH,name:"火柴"},
     {tag:TAG_LIB_LAMP,name:"酒精灯"},
     {tag:TAG_LIB_IRON,name:"铁架台"},
     {tag:TAG_LIB_SHIGUAN,name:"硬质试管"},
     ];
