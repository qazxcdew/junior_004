Shiguan = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this,10, TAG_SHIGUAN_NODE);
		this.init();
	},

	init : function(){
		var shiguan =  new Button(this, 9, TAG_SHIGUAN, "#sg.png",this.callback);
	//	shiguan.setScale(0.8);

	},	
	qiaoda:function(){
		var frames=[];
		for (i=1;i<=2;i++){
			var str = "hand"+i+".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.2);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;	
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_SHIGUAN:
			if(action==ACTION_DO1){
				hand.removehand(this,1,2);
				hand.addlefthand(this,"#hand/hand_left",cc.p(-20,20));

				var rotate = cc.rotateTo(1,-90);
				var seq = cc.sequence(rotate,cc.delayTime(0.5),cc.callFunc(function(){
					p.runAction(func);
				},this));
				this.runAction(seq);
			}
			else if(action==ACTION_DO2){
				var up  = new cc.Sprite("#iron/up.png");
				up.setPosition(cc.p(612,335));
				this.getParent().addChild(up,15);
				
				var down  = new cc.Sprite("#iron/down.png");
				down.setPosition(cc.p(612,319));
				this.getParent().addChild(down,15);
								
				hand.removehand(this,1,1);
				hand.addlefthand(this,"#hand/hand_left",cc.p(-20,60));
				var move = cc.moveTo(1.5,cc.p(470,315));
				
				var move1 = cc.moveTo(0.8,cc.p(580,323));
				var move2 = cc.moveTo(0.7,cc.p(650,330));
				var rotate = cc.rotateTo(0.5,-95);
				var seq = cc.sequence(move,cc.callFunc(function(){
					var show = new ShowTip("硬质试管口" +
							"\n略微向下",25,cc.p(850,370));
				},this),rotate,move1,cc.callFunc(function(){
					hand.addrighthand(this,"#hand/hand_right",cc.p(20,-85));	
				},this),move2,cc.delayTime(0.5),cc.callFunc(function(){
					hand.removehand(this,1,1);
					hand.removehand(this,1,2);
					this.flowNext();
				},this));
				this.runAction(seq);
			}
			else if(action==ACTION_DO3){
				var show = new ShowTip1("观察硬质试管,管壁有水" +
						"\n珠，氧化铜变成铜单质",25,cc.p(900,370),cc.p(900,390),277,120,"#gongshi.png",cc.p(900,340));
				var cuo =this.getChildByTag(TAG_SHOW2);
				cuo.runAction(cc.fadeOut(2));
				
				var shuidi = new cc.Sprite("#shuidi.png");
				shuidi.setPosition(cc.p(0,-20));
				shuidi.setOpacity(0);	
				shuidi.setRotation(90);
				this.addChild(shuidi,6); 
				shuidi.runAction(cc.fadeIn(2));
				
				var cuo1 = new cc.Sprite("#cu2.png");
				cuo1.setPosition(cc.p(-5,-90));
				cuo1.setScale(0.6);
				cuo1.setOpacity(0);	
				cuo1.setRotation(90);
				this.addChild(cuo1,6); 
				var seq = cc.sequence(cc.fadeIn(2),cc.callFunc(function(){
					this.flowNext();
				},this));
				cuo1.runAction(seq);										
			}   
			break;
		}
	},

	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		case TAG_SHIGUAN:
			if(action==ACTION_DO1){
				var hand = new cc.Sprite("#hand1.png");
				hand.setPosition(cc.p(10,-150));
				hand.setRotation(90);
				hand.setScale(0.5);
				this.addChild(hand,1,TAG_SHOW1);
			//	hand.runAction(cc.repeatForever(this.qiaoda()));
				hand.runAction(cc.repeat(this.qiaoda(),6));

				var cuo=this.getChildByTag(TAG_SHOW);
				var seq1 = cc.sequence(cc.delayTime(1),cc.fadeOut(1),cc.callFunc(function(){
					cuo.removeFromParent(true);
				},this));
				cuo.runAction(seq1);

				var cuo1 = new cc.Sprite("#cu1.png");
				//cuo1.setPosition(cc.p(-5,-105));
				//cuo1.setPosition(cc.p(-3,-65));
				cuo1.setPosition(cc.p(-5,-90));
				cuo1.setScale(0.6);
				cuo1.setOpacity(0);	
				cuo1.setRotation(90);
				this.addChild(cuo1,5,TAG_SHOW2);
				var seq2 = cc.sequence(cc.delayTime(1),cc.fadeIn(1),cc.delayTime(1),cc.callFunc(function(){
					    this.flowNext();
						hand.removeFromParent(true);
				},this));
				cuo1.runAction(seq2);

			}
			if(action==ACTION_DO2){


			}
        
            break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});