Spoon = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 9, TAG_SPOON_NODE);
		this.init();
	},

	init : function(){
		var spoon =  new Button(this, 9, TAG_SPOON, "#spoon.png",this.callback);

	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_SPOON:
			
			var cuonode = this.getParent().getChildByTag(TAG_CUO_NODE);
			cuonode.getChildByTag(TAG_CUOLID).removeFromParent(true);
			hand.addlefthand(cuonode,"#hand/hand_left",cc.p(-20,-20));
			cuonode.runAction(cc.rotateTo(0.5,60));
			
			hand.addrighthand(this,"#hand/hand_right",cc.p(65,25),0.5,-30);
			var mov = cc.moveTo(0.5,cc.p(80,50));
			var move = cc.moveTo(0.5,cc.p(20,10));
			var rotate = cc.rotateTo(0.5,10);
			var spawn = cc.spawn(mov,rotate);
			
			var move1 = cc.moveTo(0.5,cc.p(90,55));
			var ber = cc.bezierTo(1,[cc.p(180,30),cc.p(250,0),cc.p(270,-40)]);			
			var rotate1= cc.rotateTo(0.5,-30);			
			var seq = cc.sequence(spawn,move,cc.callFunc(function(){
				p.setSpriteFrame("spoon1.png");
			},this),cc.delayTime(0.5),move1,cc.callFunc(function(){
				cuonode.removeFromParent(true);
				var zhinode = this.getParent().getParent().getChildByTag(TAG_ZHI_NODE);
				hand.addlefthand(zhinode,"#hand/hand_left",cc.p(-120,-10),0.5,30);
			},this),ber,rotate1,cc.callFunc(function(){
				p.runAction(func);
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.flowNext();
				this.getParent().removeFromParent(true);
				
			},this));	
			this.runAction(seq);
		break;
		}
	},
	
	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		case TAG_SPOON:
			p.setSpriteFrame("spoon.png");
			var cuo = new cc.Sprite("#fenmo1.png");
			cuo.setPosition(cc.p(310,8));
			
			var zhinode = this.getParent().getParent().getChildByTag(TAG_ZHI_NODE);
			var zhi = zhinode.getChildByTag(TAG_ZHI);
			zhi.addChild(cuo,1,TAG_SHOW);
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});