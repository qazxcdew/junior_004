Zhi = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 9, TAG_ZHI_NODE);
		this.init();
	},

	init : function(){
		var zhi =  new Button(this, 9, TAG_ZHI, "#zhi.png",this.callback);
		zhi.setScale(0.7);

	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		func.retain();
		switch(p.getTag()){	
		case TAG_ZHI:
//			var shiguannode = this.getParent().getChildByTag(TAG_SHIGUAN_NODE);
//			hand.addrighthand(shiguannode,"#hand/hand_right",cc.p(20,20));
//
//			var rotate = cc.rotateTo(1,-90);
//			var move = cc.moveTo(1,cc.p(960,200));
//			var rotate1 = cc.rotateTo(2,0);
//			var spawn1 = cc.spawn(cc.moveTo(2,cc.p(900,200)),rotate1);
//			var seq = cc.sequence(rotate,move,spawn1);
//			shiguannode.runAction(seq);
//
//			var move1 = cc.moveTo(2,cc.p(905,200));
//			var rotate2 = cc.rotateTo(2,90);
//			var move2 = cc.moveTo(2,cc.p(900,250));          
//			var spawn = cc.spawn(move2,rotate2);
//			var move3 = cc.moveTo(1.5,cc.p(900,400));
//			var seq1 = cc.sequence(move1,spawn,cc.callFunc(function(){
//				p.runAction(func);
//			},this),move3,cc.callFunc(function(){
//				this.removeFromParent();
//				this.flowNext();
//
//			},this));
//			this.runAction(seq1);
			
			
			var shiguannode = this.getParent().getChildByTag(TAG_SHIGUAN_NODE);
			hand.addrighthand(shiguannode,"#hand/hand_right",cc.p(20,20));
			var rotate = cc.rotateTo(1,-90);
			var move = cc.moveTo(1,cc.p(900,200));
			var rotate1 = cc.rotateTo(1,0);			
			var seq = cc.sequence(rotate,cc.callFunc(function(){
				this.setVisible(false);	
				var zhi1 = new cc.Sprite("#zhi1.png");
				zhi1.setPosition(cc.p(4,386));
				zhi1.setScale(0.8);
				zhi1.setRotation(90);
				shiguannode.addChild(zhi1,8,TAG_SHOW3);

				var cuo = new cc.Sprite("#fenmo1.png");
				cuo.setPosition(cc.p(357,26));
				cuo.setScale(0.9);
				zhi1.addChild(cuo,1,TAG_SHOW);

				var move1 = cc.moveTo(1,cc.p(4,70));
				var move2 = cc.moveTo(1,cc.p(4,250));
				var seq1 = cc.sequence(move1,cc.delayTime(1),move2,cc.callFunc(function(){
					zhi1.removeFromParent(true);
		            this.flowNext();
		            this.removeFromParent(true);
				},this));
				zhi1.runAction(seq1);
				
			},this),move,rotate1,cc.callFunc(function(){
				p.runAction(func);
			},this));
			shiguannode.runAction(seq);
			break;
		}
	},

	actionDone:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){
		case TAG_ZHI:
//			var cuo = p.getChildByTag(TAG_SHOW);
//			cuo.runAction(cc.fadeOut(0.3));
//
//			var shiguannode = this.getParent().getChildByTag(TAG_SHIGUAN_NODE);
//			var cuo1 = new cc.Sprite("#cu.png");
//			cuo1.setPosition(cc.p(0,-100));
//			//cuo1.setPosition(cc.p(0,-125));
//
//
//			cuo1.setScale(0.8,1);
//			cuo1.setOpacity(0);			
//			shiguannode.addChild(cuo1,1,TAG_SHOW);
//			cuo1.runAction(cc.fadeIn(0.5));

			var shiguannode = this.getParent().getChildByTag(TAG_SHIGUAN_NODE);
			var cuo = shiguannode.getChildByTag(TAG_SHOW3).getChildByTag(TAG_SHOW);
			cuo.runAction(cc.fadeOut(0.5));

			
			

			var cuo1 = new cc.Sprite("#cu.png");
			cuo1.setPosition(cc.p(0,-100));
			cuo1.setScale(0.8,1);
			cuo1.setOpacity(0);			
			shiguannode.addChild(cuo1,1,TAG_SHOW);
			cuo1.runAction(cc.fadeIn(0.5));
		}
	},
	flowNext:function(){
		gg.flow.next();
	},

});
